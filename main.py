from app import RESOURCES_DIR
from app.ConsoleApp import ConsoleApp

if __name__ == "__main__":
    app = ConsoleApp(result_folder_path=RESOURCES_DIR, questions_file_path=RESOURCES_DIR / "Опросник.xlsx")
    app.start()
