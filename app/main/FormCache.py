import dataclasses
import json
import os
from pathlib import Path

from app import RESOURCES_DIR
from app.main.MessageModel import MessageModel


class FormCache:

    def __init__(self, temp_folder: Path = RESOURCES_DIR):
        self.cache_temp_folder = temp_folder
        self.need_cache = True
        self.filename = self.cache_temp_folder / f"cache.json"
        self.__create_cache_file()

    @staticmethod
    def delete_cache_file(cache_file: Path):
        os.remove(cache_file)

    def __create_cache_file(self):
        if not self.filename.exists():
            self.filename.touch()

    def save_message(self, message: MessageModel): ...

    def get_cache(self) -> list[MessageModel]: ...

    def delete_cache(self): ...

    def save_cache_to_file(self, message: MessageModel, form_name: str):
        forms = self.__get_cached_forms()
        form = forms.get(form_name, {})
        # print(f"до. form: {form}")
        form.update({message.id: dataclasses.asdict(message)})
        with open(self.filename, "w") as file:
            forms.update({form_name: form})
            json.dump(forms, file, indent=4, ensure_ascii=False)
        # print(f"после. form: {form}")

    def _get_cache_by_name(self, form_name: str) -> list[MessageModel]:
        forms: dict = self.__get_cached_forms()
        form: dict = forms.get(form_name, {})
        return [MessageModel(**message) for message in form.values()]

    def _delete_cache_by_name(self, form_name: str):
        forms: dict = self.__get_cached_forms()
        with open(self.filename, "w") as file:
            forms.pop(form_name)
            json.dump(forms, file, indent=4, ensure_ascii=False)

    def __get_cached_forms(self) -> dict:
        with open(self.filename, "r") as file:
            file_start = file.read(1)
            if len(file_start) > 0:
                return json.loads(file_start + file.read())
            else:
                return {}
