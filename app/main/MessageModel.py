from dataclasses import dataclass

ANY_STR = "any_str"
ANY_INT = "any_int"
ANY_DATE = "any_date"


@dataclass
class MessageModel:
    id: int
    text: str
    result_text: str
    allowed_answers: list
    answer_is_required: bool = False
    answer: str | None = None
