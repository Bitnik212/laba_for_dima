from pathlib import Path

from openpyxl import load_workbook
from openpyxl.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet

from app.form.SignUpForm import SignUpForm
from app.form.TestForm import TestForm
from app.main.MessageModel import MessageModel


class ReportService:

    SUBJECTS_SHEET_NAME = "Испытуемые"
    STATISTIC_SHEET_NAME = "Статистика"
    STATISTIC_DATA_SHEET_NAME = "Статистика_Данные"

    MAX_SUBJECTS = 100

    def __init__(self, result_folder_path: Path):
        self.result_file_path = result_folder_path
        self.__test_data: list[MessageModel] = []
        self.__user_info_data: list[MessageModel] = []

    def create_user_depression_report(self, worksheet: Worksheet):
        depression_level = 0
        max_depression_level = 3 * len(self.__test_data)
        answers_int: list[int] = [int(question.answer) for question in self.__test_data if question.answer_is_required]
        for answer_int in answers_int:
            depression_level += answer_int
        worksheet.cell(row=1, column=6, value="Уровень депресии")
        worksheet.cell(row=2, column=6, value=f"{depression_level}/{max_depression_level}")

    def create_reports(self):
        self.__user_info_data = SignUpForm().get_cache()
        self.__test_data = TestForm().get_cache()
        self.create_user_report()
        self.create_staff_report()

    def create_user_report(self):
        file_name = f"{self.__user_surname}_{self.__user_firstname}-Результаты"
        workbook = Workbook()
        worksheet: Worksheet = workbook.active
        worksheet.title = file_name
        worksheet.cell(row=1, column=4, value="Фамилия")
        worksheet.cell(row=2, column=4, value=self.__user_surname)
        worksheet.cell(row=1, column=5, value="Имя")
        worksheet.cell(row=2, column=5, value=self.__user_firstname)
        self.__render_user_result(worksheet, self.__test_data)
        self.create_user_depression_report(worksheet)
        workbook.save(self.result_file_path / f"{file_name}.xlsx")

    def create_staff_report(self):
        workbook_file_path = self.result_file_path / "Результаты.xlsx"
        if workbook_file_path.exists():
            workbook = load_workbook(workbook_file_path)
        else:
            workbook = Workbook()
            workbook = self.__init_staff_report(workbook, self.__test_data, self.__user_info_data)
        user_result_sheet_name = f"{self.__user_surname}_{self.__user_firstname}-Результаты"
        user_result_sheet = workbook.create_sheet(user_result_sheet_name)
        self.__render_user_result(user_result_sheet, self.__test_data)
        self.__append_user(workbook)
        statistic_sheet: Worksheet = workbook.get_sheet_by_name(self.STATISTIC_SHEET_NAME)
        answer_letters = {0: "B", 1: "C", 2: "D", 3: "E"}
        question_letters = {}
        [question_letters.update({index: chr(i - 973)}) for index, i in enumerate(range(1040, 1072)) if i - 975 <= 90]
        for index, question in enumerate(self.__test_data):
            row = index + 3
            statistic_sheet.cell(row=row, column=1, value=f"='Статистика_Данные'!{question_letters.get(index)}1")
            for answer in range(0, 4):
                column = answer + 2
                statistic_sheet.cell(
                    row=row,
                    column=column,
                    value=self.__statistic_function(
                        answer_letter=answer_letters.get(answer),
                        question_letter=question_letters.get(index)
                    )
                )
        workbook.save(workbook_file_path)

    def __init_staff_report(self, workbook: Workbook, questions_data: list[MessageModel], sign_up_data: list[MessageModel]):
        # Create sheets
        default_sheet: Worksheet = workbook.active
        default_sheet.title = self.STATISTIC_SHEET_NAME
        workbook.create_sheet(self.STATISTIC_DATA_SHEET_NAME, 1)
        workbook.create_sheet(self.SUBJECTS_SHEET_NAME, 2)
        # Init statistic data sheet
        statistic_data_sheet: Worksheet = workbook.get_sheet_by_name(self.STATISTIC_DATA_SHEET_NAME)
        statistic_data_sheet.cell(row=1, column=1, value="Фамилия")
        statistic_data_sheet.cell(row=1, column=2, value="Имя")
        for index, question in enumerate(questions_data):
            column = index + 3
            statistic_data_sheet.cell(row=1, column=column, value=question.result_text)
        # Init subjects sheet
        subjects_sheet: Worksheet = workbook.get_sheet_by_name(self.SUBJECTS_SHEET_NAME)
        for index, user_info in enumerate(sign_up_data):
            subjects_sheet.cell(row=1, column=index + 1, value=user_info.result_text)
        # Init statistic sheet
        statistic_sheet: Worksheet = workbook.get_sheet_by_name(self.STATISTIC_SHEET_NAME)
        statistic_sheet.cell(row=2, column=1, value="Вопрос")
        statistic_sheet.cell(row=2, column=2, value="0")
        statistic_sheet.cell(row=2, column=3, value="1")
        statistic_sheet.cell(row=2, column=4, value="2")
        statistic_sheet.cell(row=2, column=5, value="3")
        return workbook

    def __append_user(self, workbook: Workbook):
        statistic_data_sheet: Worksheet = workbook.get_sheet_by_name(self.STATISTIC_DATA_SHEET_NAME)
        self.__append_stats(statistic_data_sheet, self.__test_data)
        subjects_sheet: Worksheet = workbook.get_sheet_by_name(self.SUBJECTS_SHEET_NAME)
        row = subjects_sheet.max_row + 1
        for index, msg in enumerate(self.__user_info_data):
            column = index + 1
            subjects_sheet.cell(row=row, column=column, value=msg.answer)

    def __append_stats(self, worksheet: Worksheet, data: list[MessageModel]):
        row = worksheet.max_row + 1
        worksheet.cell(row=row, column=1, value=self.__user_surname)
        worksheet.cell(row=row, column=2, value=self.__user_firstname)
        for index, msg in enumerate(data):
            column = index + 3
            worksheet.cell(row=row, column=column, value=msg.answer)

    @staticmethod
    def __render_user_result(worksheet: Worksheet, data: list[MessageModel]):
        worksheet.cell(row=1, column=1, value="Название вопроса")
        worksheet.cell(row=1, column=2, value="Ответ")
        for index, message in enumerate(data):
            row = index + 2
            worksheet.cell(row=row, column=1, value=message.result_text)
            worksheet.cell(row=row, column=2, value=message.answer)

    @staticmethod
    def __render_user_info(worksheet: Worksheet, data: list[MessageModel]):
        for index, message in enumerate(data):
            column = index + 4
            worksheet.cell(row=1, column=column, value=message.result_text)
            worksheet.cell(row=2, column=column, value=message.answer)

    @property
    def __user_firstname(self) -> str:
        return [msg.answer for msg in self.__user_info_data if msg.result_text.startswith("Имя")][0]

    @property
    def __user_surname(self) -> str:
        return [msg.answer for msg in self.__user_info_data if msg.result_text.startswith("Фамилия")][0]

    def __statistic_function(self, answer_letter: str, question_letter: str) -> str:
        subjects_count_func = f"COUNTA('{self.SUBJECTS_SHEET_NAME}'!$A$2:'{self.SUBJECTS_SHEET_NAME}'!$A${self.MAX_SUBJECTS})"
        all_answers_by_question_func = f"COUNTIF('{self.STATISTIC_DATA_SHEET_NAME}'!${question_letter}$2:'{self.STATISTIC_DATA_SHEET_NAME}'!${question_letter}${self.MAX_SUBJECTS}, CELL(\"contents\", {answer_letter}$2) )"
        return f"=(({all_answers_by_question_func})*100)/{subjects_count_func}"
