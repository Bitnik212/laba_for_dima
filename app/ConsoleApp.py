import datetime
from pathlib import Path

from app.form.SignUpForm import SignUpForm
from app.form.TestForm import TestForm
from app.form.WelcomeForm import WelcomeForm
from app.main.FormCache import FormCache
from app.main.MessageModel import MessageModel, ANY_STR, ANY_INT, ANY_DATE
from app.main.ReportService import ReportService


class ConsoleApp:

    def __init__(self, result_folder_path: Path, questions_file_path: Path):
        self.__report_service = ReportService(result_folder_path)
        self.forms = [
            SignUpForm(),
            WelcomeForm(),
            TestForm(questions_file_path)
        ]

    def start(self):
        for form in self.forms:
            # print(f"form.need_cache: {form.need_cache}, form: {form.form_name}")
            skip_cached_messages = True
            cached_form_ids = []
            if form.need_cache:
                cached_form = form.get_cache()
                cached_form_ids = [msg.id for msg in cached_form]
                if 0 < len(cached_form) < form.message_count:
                    skip_cached_messages: bool = bool(input("Продолжить ? Да / Нет\n> ").lower().count("да"))
                elif len(cached_form) == form.message_count:
                    skip_cached_messages: bool = not bool(input("Пройти заново ? Да / Нет\n> ").lower().count("да"))
                if not skip_cached_messages:
                    form.delete_cache()
            # print(f"skip_cached_messages: {skip_cached_messages}")
            for message_number in range(form.message_count):
                message = form.get_message(number=message_number)
                if message is not None:
                    if skip_cached_messages and message.id in cached_form_ids:
                        continue
                    print("--"*50)
                    message = self.print_message(message)
                    if form.need_cache:
                        form.save_message(message)
                else:
                    print("Ошибка при получении сообщения")
        self.__report_service.create_reports()
        FormCache.delete_cache_file(FormCache().filename)
        input("Спасибо\nНажмите enter чтобы закрыть программу\n")

    def print_message(self, message: MessageModel) -> MessageModel:
        print(message.text)
        if message.answer_is_required:
            if len(message.allowed_answers) > 0:
                answer = input(f"Введите ответ {self.get_allowed_values_help(message)}:\n> ")
            else:
                answer = input("Введите ответ:\n> ")
            if self.is_valid_answer(message, answer):
                message.answer = answer
                return message
            else:
                print(f"\n --- Введите валидный ответ! ---")
                return self.print_message(message)

    @classmethod
    def is_valid_answer(cls, message: MessageModel, answer: str) -> bool:
        is_valid = False
        if answer in message.allowed_answers:
            is_valid = True
        if ANY_STR in message.allowed_answers:
            is_valid = True
        if ANY_INT in message.allowed_answers:
            try:
                int(answer)
                is_valid = True
            except ValueError:
                is_valid = False
        if ANY_DATE in message.allowed_answers:
            try:
                datetime.date.fromisoformat(answer)
                is_valid = True
            except ValueError as e:
                print(e)
                is_valid = False
        return is_valid

    @classmethod
    def get_allowed_values_help(cls, message: MessageModel) -> str:
        help_list = []
        if ANY_STR in message.allowed_answers:
            help_list.append("[любая строка]")
        if ANY_INT in message.allowed_answers:
            help_list.append("[любое число]")
        if ANY_DATE in message.allowed_answers:
            help_list.append("[yyyy-mm-dd]")
        if len(message.allowed_answers) > 1:
            help_list.append(str(message.allowed_answers))
        return ", ".join(help_list)
