from app.form.ApplicationForm import ApplicationForm
from app.main.MessageModel import MessageModel


class WelcomeForm(ApplicationForm):

    def __init__(self):
        super().__init__()
        self.form_name: str = __name__
        self.need_cache: bool = False
        self.message_count = 1

    WELCOME_TEXT = """
    Этот опросник состоит из 21 группы утверждений. Пожалуйста, внимательно прочитайте каждую группу утверждений и, 
    затем выберите одно утверждение в каждой группе, которое наилучшим образом описывает то, 
    как вы себя чувствовали в течение последней недели, в том числе и сегодня. Обведите цифру рядом с утверждением, которое вы выбрали. 
    Если несколько утверждений в группе одинаково хорошо описывают ваше состояние, обведите наибольшее число для этой группы. 
    Убедитесь, что вы не выбираете больше одного утверждения для любой группы, включая пункт 19 (Потеря веса).
    Если все понятно, то нажмите enter, чтобы начать тест
    """

    def get_message(self, number: int) -> MessageModel:
        return MessageModel(1, self.WELCOME_TEXT, "", [""], True)
