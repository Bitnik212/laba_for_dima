from pathlib import Path

import openpyxl

from app import RESOURCES_DIR
from app.form.ApplicationForm import ApplicationForm
from app.main.MessageModel import MessageModel


class TestForm(ApplicationForm):

    def __init__(self, questions_file_path: Path = RESOURCES_DIR / "Опросник.xlsx"):
        super().__init__()
        self.__questions: dict = self.__load_questions(questions_file_path)
        self.form_name: str = __name__
        self.message_count = len(self.__questions.keys())

    @staticmethod
    def __load_questions(questions_file_path: Path) -> dict:
        xl = openpyxl.load_workbook(questions_file_path).active
        result = {}
        for row in range(1, xl.max_row):
            values = [col[row].value for col in xl.iter_cols(1, xl.max_column)]
            text = f"\n{values[0]}\n\nВозможные ответы:\n"
            allowed_answers = values[2:6]
            for answer in allowed_answers:
                text += f" - {answer}\n"
            result.update({
                (row - 1): MessageModel(
                    id=row,
                    text=text,
                    result_text=values[1],
                    allowed_answers=["0", "1", "2", "3"],
                    answer_is_required=True
                )
            })
        return result

    def get_message(self, number: int) -> MessageModel | None:
        return self.__questions.get(number, None)

    def save_message(self, message: MessageModel):
        self.save_cache_to_file(message, self.form_name)

    def get_cache(self) -> list[MessageModel]:
        return self._get_cache_by_name(self.form_name)

    def delete_cache(self):
        self._delete_cache_by_name(form_name=self.form_name)
