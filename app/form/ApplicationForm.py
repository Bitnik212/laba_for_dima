from app.main.FormCache import FormCache
from app.main.MessageModel import MessageModel


class ApplicationForm(FormCache):

    def __init__(self):
        super().__init__()
        self.form_name: str = __name__

    message_count: int = 0

    def get_message(self, number: int) -> MessageModel: ...
