from app.form.ApplicationForm import ApplicationForm
from app.main.MessageModel import MessageModel, ANY_STR, ANY_INT, ANY_DATE


class SignUpForm(ApplicationForm):

    def __init__(self):
        super().__init__()
        self.messages: dict = {
            0: MessageModel(1, "Ваше Имя", "Имя", [ANY_STR], True),
            1: MessageModel(2, "Ваша Фамилия", "Фамилия", [ANY_STR], True),
            2: MessageModel(3, "Ваш рост", "Рост", [ANY_INT], True),
            3: MessageModel(4, "Ваше возраст", "Возраст", [ANY_INT], True),
            4: MessageModel(5, "Ваш пол", "пол", ["М", "Ж"], True),
            5: MessageModel(6, "Ваша дата рождения", "Дата рождения", [ANY_DATE], True),
        }
        self.message_count = len(self.messages)
        self.form_name: str = __name__

    def get_message(self, number: int) -> MessageModel:
        return self.messages.get(number)

    def save_message(self, message: MessageModel):
        self.save_cache_to_file(message, self.form_name)

    def get_cache(self) -> list[MessageModel]:
        return self._get_cache_by_name(self.form_name)

    def delete_cache(self):
        self._delete_cache_by_name(form_name=self.form_name)
