from pathlib import Path

ROOT_DIR: Path = [dir for dir in Path().iterdir() if dir.name == "app"][0].absolute().parent

APP_DIR: Path = ROOT_DIR / "app"

RESOURCES_DIR: Path = ROOT_DIR / "resources"
